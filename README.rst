Linode DNS Sync
===============

Sync Linode managed DNS zones from YAML files.

This project is not supported by or affiliated with Linode_.

.. _linode: https://www.linode.com


Install & use
-------------

Install the package using::

   pip3 install git+https://gitlab.com/beli-sk/linode-dns-sync.git

Create environment variable ``LINODE_TOKEN`` containing your Linode API token.

Create your zone definition in YAML format similar to::

   domain: example.com
   type: master
   records:
   - name: ""
     type: MX
     priority: 10
     target: mail.example.com
     ttl_sec: 3600
   - name: www
     type: A
     target: "127.0.0.1"
   - name: www-old
     type: A
     state: absent

Run the program providing your zone YAML file(s) on the command line::

   linode-dns-sync zone.yaml anotherzone.yaml


Locations
---------

The `project page`_ is hosted on GitLab.com.

If you find something wrong or know of a missing feature, please
`create an issue`_ on the project page.

.. _project page:         https://gitlab.com/beli-sk/linode-dns-sync
.. _create an issue:      https://gitlab.com/beli-sk/linode-dns-sync/issues


License
-------

.. include:: <isonum.txt>

Copyright |copy| 2020 Michal Belica <code@beli.sk>

::

    Linode DNS Sync is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    Linode DNS Sync is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with Linode DNS Sync.  If not, see < http://www.gnu.org/licenses/ >.

A copy of the license can be found in the ``LICENSE`` file in the
distribution.
