#!/usr/bin/env python3
from setuptools import setup, find_packages
from codecs import open # To use a consistent encoding
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

# get version number
defs = {}
with open(path.join(here, 'linode_dns_sync/defs.py')) as f:
    exec(f.read(), defs)

setup(
    name='linode_dns_sync',
    version=defs['__version__'],
    description=defs['app_description'],
    long_description=long_description,
    url='https://gitlab.com/beli-sk/linode-dns-sync',
    author="Michal Belica",
    author_email="code@beli.sk",
    license="GPL-3",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        ],
    keywords=['docker', 'container'],
    zip_safe=True,
    install_requires=[
        'linode_api4',
        'pyyaml',
        ],
    packages=['linode_dns_sync'],
    entry_points={
        'console_scripts': [
            'linode-dns-sync = linode_dns_sync:main',
            ],
        },
    )
