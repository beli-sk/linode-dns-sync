__version__ = '0.0.1'
app_name = 'Linode DNS Sync'
app_description = 'Sync Linode managed DNS zones from YAML files.'
app_version = '{} {}'.format(app_name, __version__)
app_name_desc = '{} - {}'.format(app_name, app_description)
