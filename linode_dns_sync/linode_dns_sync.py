import os
import yaml
import argparse
from linode_api4 import LinodeClient

from .defs import *


DEFAULT_TTL = 300
DEFAULT_EMAIL = "admin@coders.sk"
LINODE_TTL_STEPS = [
        300, 60*60, 2*60*60, 4*60*60, 8*60*60, 16*60*60, 24*60*60,
        2*24*60*60, 4*24*60*60, 7*24*60*60, 14*24*60*60, 28*24*60*60,
        ]

type_required_fields = {
        'MX': ('target', 'priority'),
        'A': ('target',),
        'AAAA': ('target',),
        'CNAME': ('target',),
        'NS': ('target',),
        'TXT': ('target',),
        'CAA': ('target', 'tag'),
        }

domains = []
domains_index = []

def align_ttl(ttl):
    for ttl_step in LINODE_TTL_STEPS:
        if ttl_step >= ttl:
            return ttl_step
    return LINODE_TTL_STEPS[-1]

def make_records_index(domain, by=('name', 'type')):
    index = {}
    for record in domain['records']:
        key = tuple([record[x] for x in by])
        if key not in index:
            index[key] = [record]
        else:
            index[key].append(record)
    return index

def records_match(record, linode_record):
    if record['name'] != linode_record.name or record['type'] != linode_record.type:
        return False
    if 'state' in record and record['state'] == "absent":
        return False
    # TODO catch unsupported types
    for field in type_required_fields[record['type']]:
        if record[field] != getattr(linode_record, field):
            return False
    if 'ttl_sec' not in record:
        if linode_record.ttl_sec != DEFAULT_TTL:
            return False
    else:
        if align_ttl(record['ttl_sec']) != linode_record.ttl_sec:
            return False
    return True

def sync_records(domain, linode_domain):
    print(f"Syncing records for domain {domain['domain']}")
    records_index = make_records_index(domain)
    items_clean = []
    linode_records_to_delete = []
    for linode_record in linode_domain.records:
        key = (linode_record.name, linode_record.type)
        if key not in records_index:
            print(f'Record {key} not managed, skipping')
            continue
        # try to find match
        for record in records_index[key]:
            print(f'Matching {key}')
            if records_match(record, linode_record):
                print(f'Record {key} up to date')
                items_clean.append(linode_record.id)
                records_index[key].remove(record)
                break
        else:
            if record['type'] == 'CNAME':
                print(f'Record {key} did not match, updating CNAME target')
                linode_record.target = record['target']
                if 'ttl_sec' not in record:
                    linode_record.ttl_sec = DEFAULT_TTL
                else:
                    linode_record.ttl_sec = record['ttl_sec']
                linode_record.save()
                records_index[key].remove(record)
            else:
                print(f'Record {key} did not match')
                linode_records_to_delete.append(linode_record)
    # create new records
    for key, records in records_index.items():
        for record in records:
            if 'state' in record and record['state'] == 'absent':
                continue
            print(f'Creating record {key} {record["target"]}')
            if 'ttl_sec' not in record:
                record['ttl_sec'] = DEFAULT_TTL
            linode_record = linode_domain.record_create(record_type=record['type'], **record)
    # delete unmatched records
    for linode_record in linode_records_to_delete:
        print(f'Deleting record {linode_record.name} {linode_record.type} {linode_record.target}')
        linode_record.delete()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('zone_files', metavar='ZONE_YAML', nargs='+')
    args = parser.parse_args()

    linode_token = os.getenv("LINODE_TOKEN")
    if not linode_token:
        parser.error("Environment variable LINODE_TOKEN missing.")

    # populate domains from files
    try:
        for zone_file in args.zone_files:
            with open(zone_file, 'r') as f:
                for data in yaml.safe_load_all(f):
                    domains.append(data)
    except FileNotFoundError as e:
        parser.error(str(e))
    domains_index = dict([(x["domain"], x) for x in domains])

    client = LinodeClient(linode_token)
    linode_domains = client.domains()

    # write down IDs for existing domains
    for linode_domain in linode_domains:
        if linode_domain.domain in domains_index:
            print(f"Found domain {linode_domain.domain}")
            domains_index[linode_domain.domain]["id"] = linode_domain.id
            sync_records(domains_index[linode_domain.domain], linode_domain)

    missing_domain_names = [x["domain"] for x in domains if "id" not in x]
    if missing_domain_names:
        print("Create missing domains:")
        for missing_domain in missing_domain_names:
            print(f"{missing_domain}")
            linode_domain = client.domain_create(
                    missing_domain,
                    master=True,
                    ttl_sec=DEFAULT_TTL,
                    soa_email = DEFAULT_EMAIL,
                    )
            domains_index[missing_domain]["id"] = linode_domain.id
            sync_records(domains_index[missing_domain], linode_domain)


if __name__ == "__main__":
    main()
